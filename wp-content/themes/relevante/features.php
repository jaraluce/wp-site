<?php 
/**
 * 	Template Name: Features
*/
get_header(); the_post(); ?>
<div class="wrapper hiring">
	<article class="post"> 
		<h1 class="title"><?php  the_title(); ?>AA</h1>
		<div class="the-content">
		<?php the_content(); ?>
			<?php $offers = CFS()->get('job_offers');
			 if($offers){ ?>
			<div id="job-offers"> 
				<?php foreach($offers as $o){ ?> 
				<div class="offer">
					<h2>&#8649; <?php echo $o['job_title']; ?></h2>
					<?php echo $o['job_desciption']; ?>
				</div>
				<?php } ?> 
			</div>
			<?php } ?>
			<div class="aligncenter">
				<button type="button" id="hd-login" onclick="userengage('event.showWatingListForm')"><?php _e('Request invitation','relevante'); ?></button>
				
<!-- 				<a class="linkedin-signin-btn end" href="<?php echo CFS()->get('sign_in_url', get_option('page_on_front')); ?>">
					<span>&nbsp;<i></i></span><b><?php _e('Sign in with LinkedIn','relevante'); ?></b>
				</a> -->
			</div>
		</div>
	</article>
</div>
<?php get_footer(); ?>

<script type="text/javascript">
$(document).ready(function(){
    mixpanel.track("Features visit");

    $("#card-glance").click(function(){
    	console.log('HI')
		$("#card-glance").slideDown();
		$("#card-detail").slideUp();
	});
});
</script>