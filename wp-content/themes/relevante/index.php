<?php get_header(); ?>
<div class="wrapper blogposts">
	<?php if ( have_posts() ) :  ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<article class="post">
				<h1 class="title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
				</h1>
				<div class="post-meta">
					<?php _e('Posted on','break'); ?> 
					<?php the_time('m/d/Y'); ?> <?php _e('in','break'); ?>
					<span class="category"><?php echo get_the_category_list(', '); ?></span> |
					<?php if( comments_open() ) : ?>
						<span class="comments-link">
							<?php comments_popup_link( __( 'No Comment', 'break' ), __( '1 Comment', 'break' ), __( '% Comments', 'break' ) );  
							?>
						</span>
					<?php endif; ?>
				</div>
				<div class="the-content"> 
					<?php 
					if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					}
					the_excerpt( 'Continue...' ); ?> 
					<?php wp_link_pages(); ?>
				</div>
				<?php /*<div class="meta clearfix">
					<div class="category"><?php echo get_the_category_list(); ?></div>
					<div class="tags"><?php echo get_the_tag_list( '| &nbsp;', '&nbsp;' ); ?></div>
				</div>*/ ?>
			</article>
		<?php endwhile; ?>
		
		<div id="pagination" class="clearfix">
			<div class="past-page"><?php previous_posts_link( 'newer' );  ?></div>
			<div class="next-page"><?php next_posts_link( 'older' ); ?></div>
		</div>
	<?php else : ?>
		<article class="post error">
			<h1 class="404"><?php _e('Nothing has been posted like that yet','relevante'); ?></h1>
		</article>
	<?php endif; ?>
	</div>
<?php get_footer(); ?>

<script type="text/javascript">
$( document ).ready(function() {
    mixpanel.track("Blog: main page visit");
});
</script>