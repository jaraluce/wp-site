<?php

add_filter('wp_mail_content_type', create_function('', 'return "text/html"; '));
define('HOME_URL', get_home_url());
define('THEME_URL', esc_url(get_bloginfo('template_url')));
define( 'RELEVANTE_VERSION', 1.0 );

function parse_shortcode($v){
	return str_replace(array('[HOME_URL]','[THEME_URL]'), array(HOME_URL, THEME_URL), $v);
}

function new_excerpt_more( $more ) {
	return '<br><a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'your-text-domain') . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');
add_theme_support( 'post-thumbnails' ); 
add_theme_support( 'automatic-feed-links' );
register_nav_menus( 
	array(
		'primary'	=>	__( 'Primary Menu', 'relevante' ),
	)
);

/* sidebar */
function relevante_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar',
		'name' => 'Sidebar',
		'description' => 'Blog sidebar!',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="side-title">',
		'after_title' => '</h3>',
		'empty_title'=> '',
	));

        register_sidebar(array(
                'id' => 'footer',
                'name' => 'footer',
                'description' => 'Blog footer!',
                'before_widget' => '',
                'after_widget' => '',
                'before_title' => '',
                'after_title' => '',
                'empty_title'=> '',
        ));

} 
add_action( 'widgets_init', 'relevante_register_sidebars' );


/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/

function relevante_scripts()  { 

	wp_enqueue_style( 'relevante-font', 'https://fonts.googleapis.com/css?family=Lato:400,700,300', '10005', 'all' );
	wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize.min.css', '10004', 'all' );
	wp_enqueue_style( 'relevante-style', get_template_directory_uri() . '/style.css', '10003', 'all' );

	wp_enqueue_script( 'jquery-cdn', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', array( 'jquery' ), RELEVANTE_VERSION, true );
	
	//wp_enqueue_script( 'jquery-mousewheel', get_template_directory_uri() . '/js/jquery.mousewheel-3.1.12.js', array(), RELEVANTE_VERSION, true );
		
	wp_enqueue_script( 'jquery-jcarousellite', get_template_directory_uri() . '/js/jquery.jcarousellite.min.js', array(), RELEVANTE_VERSION, true );
	
	wp_enqueue_script( 'jquery-script', get_template_directory_uri() . '/js/script.js', array(), RELEVANTE_VERSION, true );
 
  
}
add_action( 'wp_enqueue_scripts', 'relevante_scripts' );

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );

function bloginfo_shortcode( $atts ) {
    extract(shortcode_atts(array(
        'key' => '',
    ), $atts));
    return get_bloginfo($key);
}
add_shortcode('bloginfo', 'bloginfo_shortcode');
add_filter('the_content', 'do_shortcode');
