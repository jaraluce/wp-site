<?php get_header(); ?>
<div class="wrapper">
	<?php if ( have_posts() ) : ?> 
		<?php while ( have_posts() ) : the_post();  ?>
			<article class="post">
				<h1 class="title"><?php the_title(); ?></h1>
				<div class="the-content">
					<?php the_content();?>
					<?php wp_link_pages();  ?>
				</div>
			</article>
		<?php endwhile; ?>
	<?php else :  ?>
		<article class="post error">
			<h1 class="404"><?php _e('Nothing posted yet','relevante'); ?></h1>
		</article>
	<?php endif;?>
</div>
<?php get_footer(); ?>