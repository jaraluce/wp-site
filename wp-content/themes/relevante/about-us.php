<?php 
/**
 * 	Template Name: About Us
*/
get_header(); ?>
<div class="wrapper about">
	<article class="post">
		<h1 class="title big-center"><?php echo CFS()->get('about_title'); ?></h1>
		<div class="the-content">
			<div class="part">
				<img src="<?php echo THEME_URL ?>/images/1.png" class="n">
				<h2><?php echo CFS()->get('about_part1_title'); ?></h2>
				<p><?php echo CFS()->get('about_part1_text'); ?></p>
			</div>
			<div class="part middle">
				<img src="<?php echo THEME_URL ?>/images/2.png" class="n">
				<h2><?php echo CFS()->get('about_part2_title'); ?></h2>
			<?php $members = CFS()->get('team_members');
			 if($members){ ?>
			<div id="our-team"> 
				<?php foreach($members as $m){ ?> 
				<div class="member">
					<img src="<?php echo $m['member_image']; ?>">
					<div class="infos">
						<h4><?php echo $m['member_name']; ?></h4>
						<span><?php echo $m['member_job_title']; ?></span>
						<?php echo $m['member_description']; ?>
						<div class="social">
							<?php if($m['member_facebook_profil']){ ?><a target="blank" href="<?php echo $m['member_facebook_profil']; ?>" class="fb"></a><?php } if($m['member_twitter_profil']){ ?><a target="blank" href="<?php echo $m['member_twitter_profil']; ?>" class="tw"></a><?php } if($m['member_linkedin_profil']){ ?><a target="blank" href="<?php echo $m['member_linkedin_profil']; ?>" class="in"></a><?php } ?>
						</div>
					</div>
				</div>
				<?php } ?> 
			</div>
			<?php } ?>
			</div>
			<div class="part">
				<img src="<?php echo THEME_URL ?>/images/3.png" class="n">
				<h2><?php echo CFS()->get('about_part3_title'); ?></h2>
				<p><?php echo CFS()->get('about_part3_text'); ?></p>
			</div>
			<?php the_content();?>
			
 			<!-- <button type="button" id="hd-login" onclick="userengage('event.showWatingListForm')"><?php _e('Request invitation','relevante'); ?></button> -->

			<!-- <a class="linkedin-signin-btn end" href="<?php echo CFS()->get('sign_in_url', get_option('page_on_front')); ?>">
				<span>&nbsp;<i></i></span><b><?php _e('Sign in with LinkedIn','relevante'); ?></b>
			</a> -->
			<a href="<?php echo CFS()->get('sign_in_url', get_option('page_on_front')); ?>" id="hd-login"><?php _e('Try it free now','relevante'); ?></a>
		</div>
	</article>
</div>
<?php get_footer(); ?>

<script type="text/javascript">
$( document ).ready(function() {
    mixpanel.track("About us visit");
});
</script>