(function($) {
  $.fn.menumaker = function(options) {
      var relmenu = $(this), settings = $.extend({
        title: "Menu",
        format: "dropdown",
        sticky: false
      }, options);
      return this.each(function() {
        relmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
        $(this).find("#menu-button").on('click', function(){
          $(this).toggleClass('menu-opened');
          var mainmenu = $(this).next('ul');
          if (mainmenu.hasClass('open')) { 
            mainmenu.hide().removeClass('open');
          }
          else {
            mainmenu.show().addClass('open');
            if (settings.format === "dropdown") {
              mainmenu.find('ul').show();
            }
          }
        });
        relmenu.find('li ul').parent().addClass('has-sub');
        multiTg = function() {
          relmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
          relmenu.find('.submenu-button').on('click', function() {
            $(this).toggleClass('submenu-opened');
            if ($(this).siblings('ul').hasClass('open')) {
              $(this).siblings('ul').removeClass('open').hide();
            }
            else {
              $(this).siblings('ul').addClass('open').show();
            }
          });
        };
        if (settings.format === 'multitoggle') multiTg();
        else relmenu.addClass('dropdown');
        if (settings.sticky === true) relmenu.css('position', 'fixed');
        resizeFix = function() {
          if ($( window ).width() > 768) {
            relmenu.find('ul').show();
          }
          if ($(window).width() <= 768) {
            relmenu.find('ul').hide().removeClass('open');
          }
        };
        resizeFix();
        return $(window).on('resize', resizeFix);
      });
  };

  
$(document).ready(function() {
	$("#relmenu").menumaker({
		title: "Menu",
		format: "multitoggle"
	});
	
	$("#relmenu").prepend("<div id='menu-line'></div>");
	var foundActive = false, activeElement, linePosition = 0, menuLine = $("#relmenu #menu-line"), lineWidth, defaultPosition, defaultWidth;
	$("#relmenu > ul > li").each(function() {
	  if ($(this).hasClass('active')) {
		activeElement = $(this);
		foundActive = true;
	  }
	});
	if (foundActive === false) {
	  activeElement = $("#relmenu > ul > li").first();
	}
	defaultWidth = lineWidth = activeElement.width();
	defaultPosition = linePosition = activeElement.position();
	menuLine.css("width", lineWidth);
	menuLine.css("left", linePosition);
	$("#relmenu > ul > li").hover(function() {
	  activeElement = $(this);
	  lineWidth = activeElement.width();
	  linePosition = activeElement.position().left;
	  menuLine.css("width", lineWidth);
	  menuLine.css("left", linePosition);
	}, function() {
	  menuLine.css("left", defaultPosition);
	  menuLine.css("width", defaultWidth);
	});
	
	if($("#main-slider")[0]){
		$("#main-slider").jCarouselLite({ 
			mouseWheel: false,
			vertical: true,
			speed: 1000,
			circular: false,
			visible: 1,
			btnGo: [".dotes .1", ".dotes .2", ".dotes .3", ".dotes .4"]
		});
	}
	if($(".testimonials-slider")[0]){
		$(".testimonials-slider").jCarouselLite({
			btnNext: "#testimonials .next",
			btnPrev: "#testimonials .prev"
		});	
	}
	if($('.two-col-slide')[0]){
		twoColSwitchInter = setInterval(function(){
			$('.two-col-slide .titles h3').not('.active').trigger('click');
		}, 3000);
		$('.titles h3').click(function(e){
			$(this).parents('.two-col-slide').find('.active').removeClass('active');
			$(this).addClass('active');
			$('.'+$(this).data('id')).addClass('active');
			if (e.originalEvent !== undefined){ clearInterval(twoColSwitchInter); }
		});
		

	}
	$('#featured-video .play').click(function(){
		$('#featured-video .poster').fadeOut('slow', function(){
			$('#featured-video video').get(0).play();
			$('#featured-video video').fadeIn('slow');
		});
	});
 
function startReachInAnimation(){
!jQuery.easing && (jQuery.easing = {});
!jQuery.easing.easeOutQuad && (jQuery.easing.easeOutQuad = function( p ) { return 1 - Math.pow( 1 - p, 2 ); });

var circleController = {
  create: function( circle ){
    var obj = {
      angle: circle.data('angle'),
      element: circle,
      measure: $('<div />').css('width', 360 * 8 + parseFloat(circle.data('angle'))),
      update: circleController.update,
      reposition: circleController.reposition,
    };
    obj.reposition();
    return obj;
  },
  update: function( angle ){
    this.angle = angle;
    this.reposition();
  },
  reposition: function(){
    var radians = this.angle * Math.PI / 180, radius = ($(window).width()>360?400:300) / 2;
    this.element.css({
      marginLeft: (Math.sin( radians ) * radius - 50) + 'px',
      marginTop: (Math.cos( radians ) * radius - 50) + 'px'
    });
  }
};
var imgProfilReach$ = $('.reach-in-animation .profil .img');
var  stepProfilReach$ = imgProfilReach$.data('n')?imgProfilReach$.data('n'):0;
var spin = {
  circles: [],
  restart: function(){
  	$.each(this.circles, function(i, circle){
  		circle.measure.finish();
  		circle.measure.clearQueue();
  	});
  	spin.prep($('.reach-in-animation .circle'));
  },
  left: function(){
	  // change profil img
	  imgProfilReach$.removeClass('p'+stepProfilReach$);
	  stepProfilReach$++;
	  if(stepProfilReach$==6){ stepProfilReach$ = 0;}
	  imgProfilReach$.data('n',stepProfilReach$)
	  imgProfilReach$.hide().addClass('p'+stepProfilReach$).fadeIn(1500);
	 
	  var self = this;
	  $.each(this.circles, function(i, circle){
		m = parseInt(circle.element.css('margin-left'), 10);
		if(m==-207 || m==18 || m==149){ circle.element.addClass('active');}
		else{ circle.element.removeClass('active'); }
		circle.measure.stop(true, false).animate(
			{ 'width': '-=72' },{
				easing: 'easeOutQuad',
				duration: 2000,
				step: function( now ){ circle.update( now );   },
				complete:function(){  }
			}
		);
		});
  },
  prep: function( circles ){
    for ( var i=0, circle; i<circles.length; i++ ) {
      this.circles.push(circleController.create($(circles[i])));
    }
  }
};
	setInterval(function(){  spin.left(); }, '2500');
	setInterval(function(){ spin.restart(); }, '60000');
	spin.prep($('.reach-in-animation .circle'));
} //startReachInAnimation

function discoveryAnimation() {
	var delay = 1500;
/*	if(discoveryStep==0){
		setTimeout(function(){
			shutter(); 
		},  2900);
		setTimeout(function(){ 
			$('.discovery-animation').attr('class','discovery-animation');
		},  3700);
	}else if(discoveryStep==1){ delay = 2200; } */
	$('.discovery-animation').attr('class','discovery-animation step'+discoveryStep);
	if(discoveryStep==3){ setTimeout(function(){
		$('.discovery-animation').addClass('m');
	}, 500); }
	
	if (discoveryStep === 2) { // TYPE WRITE EFFECT.
		var str = $('.input-big-data input').data('txt'),
			i = 0, isTag, text; 
			function bigDataTextType() {
				text = str.slice(0, i++);
				$('.input-big-data input').val( text );
				if (text === str) { return; }
				setTimeout(bigDataTextType, 150);
			};
			bigDataTextType();
	}
	if (discoveryStep === 3) { return; }
    setTimeout(discoveryAnimation, delay);
	discoveryStep++;
};

function unifiedContactsAnimation(){
	setTimeout(function(){
		$('.unified-contacts-animation .recent-shares').fadeIn(2500);
	}, 500);
	setTimeout(function(){
		$('.unified-contacts-animation .bio').fadeIn(2500);
	}, 1500);	
	setTimeout(function(){
		$('.unified-contacts-animation .interactions').fadeIn(2500);
	}, 2500);
	setTimeout(function(){
		$('.unified-contacts-animation .shared-topics').fadeIn(2500);
	}, 3500);

}

/*function shutter(){
	var n = 0; var inter = setInterval(function(){
		if(n<6){ $('.shutter .frame').attr('class', 'frame f'+n);
		}else{ $('.shutter .frame').attr('class', 'frame f'+(11-n)); }
		n++; if( n==13){ clearInterval(inter);}
	}, 90);
}*/

function isScrolledIntoView(elem){
    var $elem = $(elem);
    var $window = $(window);
    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();
    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();
    return  (docViewBottom+50 >= elemTop);
}

var startedDiscoveryAnimation = false;
var startedReachInAnimation = false;
var startedUnifiedContactAnimation = false;
var discoveryStep = 2; 

$(window).ready(function(){
		console.log('UNIFIED')
		console.log($('.reach-in-animation')[0])
	if($('.reach-in-animation')[0]){
		if(!startedReachInAnimation && isScrolledIntoView('.reach-in-animation')){
			startedReachInAnimation =  true;
			startReachInAnimation();
		}
	}
});

$(window).scroll(function(){
	if($('.reach-in-animation')[0]){
		if(!startedReachInAnimation && isScrolledIntoView('.reach-in-animation')){
			startedReachInAnimation =  true;
			startReachInAnimation();
		}
		if(!startedDiscoveryAnimation && isScrolledIntoView('.discovery-animation')){
			startedDiscoveryAnimation =  true;
			setTimeout(discoveryAnimation, 700);
		}
		// if(!startedUnifiedContactAnimation && isScrolledIntoView('.unified-contacts-animation')){
		// 	unifiedContactsAnimation();
		// 	startedUnifiedContactAnimation =  true;
		// }
	}
});

// Show monthly pricing table
$('#month-switch').on('click mouseover', function(){
	$(this).siblings().removeClass('active');
	$(this).addClass('active');
	$('#year-pricing').hide();
	$('#month-pricing').show();
});

// Show yearly pricing table
$('#year-switch').on('click mouseover', function(){
	$(this).siblings().removeClass('active');
	$(this).addClass('active');
	$('#month-pricing').hide();
	$('#year-pricing').show();
});

}); // ready
 
})(jQuery);
