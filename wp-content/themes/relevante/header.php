<!DOCTYPE html> <?php //language_attributes(); ?>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
   <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title> 
	<?php wp_head(); ?>
	<link rel="icon" href="../favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
	<script src="https://use.fontawesome.com/2a44bcfed2.js"></script>
	<!-- start Mixpanel --><script type="text/javascript">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f)}})(document,window.mixpanel||[]);
mixpanel.init("dc2e431c6e52e50a035e106a78e663c2");</script><!-- end Mixpanel -->
	<!-- TruConversion for relevante.me -->
	<script type="text/javascript">
	    var _tip = _tip || [];
	    (function(d,s,id){
	        var js, tjs = d.getElementsByTagName(s)[0];
	        if(d.getElementById(id)) { return; }
	        js = d.createElement(s); js.id = id;
	        js.async = true;
	        js.src = d.location.protocol + '//app.truconversion.com/ti-js/1806/0dea5.js';
	        tjs.parentNode.insertBefore(js, tjs);
	    }(document, 'script', 'ti-js'));
	</script>
	<!-- Hotjar Tracking Code for relevante.me -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:821958,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<!-- Google Analytics -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-40737274-1', 'auto');
	  ga('send', 'pageview');

	</script>
</head>

<body <?php body_class(); ?>>

<!-- LinkedIn tracking hack Larisa -->
<img class="linkedinTracking" src="https://www.linkedin.com/profile/view?authToken=zRgB&authType=name&id=AAMAABnHmhcBniRtbAWREbFI7OukOGsbPr22Yh4" />
<!-- LinkedIn tracking hack Sofía -->
<img class="linkedinTracking" src="https://www.linkedin.com/profile/view?authToken=zRgB&authType=name&id=AAIAABuvLUIB1wuggC2hb8AgePLexKBYE0duiQ4" />

<div id="relmenu-wrapper">
<div class="wrapper">
	<div id='header'>
		<a href="<?php echo HOME_URL; ?>" id="logo"></a> 
		<div id='relmenu'>
		<?php 
			$m = wp_nav_menu( array( 'container' => '', 'theme_location' => 'primary','echo'=>false ) ); 
			echo str_replace('http://SIGN_IN_URL',CFS()->get('sign_in_url', get_option('page_on_front')), $m);
			/*  ?>
		<ul>
		   <li><a href='#'>About</a></li>
		   <li><a href='#'>Blog</a></li>
		   <li><a href='#'>Contact</a></li>
		   <li><a href='#'>Hiring Now!!</a></li>
		   <li class="mobile-only sign"><a href='#'><?php _e('SIGN IN','relevante'); ?></a></li>
		</ul>
		<?php */ ?>
		</div>
		
		<!-- <a href="#" onclick="userengage('event.showWatingListForm')" id="hd-login"><?php _e('SIGN IN','relevante'); ?></a> -->
		<!-- <button type="button" id="hd-login" class="join-waitlist"><?php _e('SIGN IN','relevante'); ?></button> -->
		<a href="<?php echo CFS()->get('login_url', get_option('page_on_front')); ?>" id="hd-login"><?php _e('LOGIN','relevante'); ?></a>
		<?php $langs = icl_get_languages('skip_missing=0'); ?>
		<div class="lang-switcher">
			<?php foreach($langs as $l){ if($l['active']){ ?>
				<a href="<?php echo $l['url']; ?>" class="current"><span class="flag"><img src="<?php echo $l['country_flag_url']; ?>"></span></span><?php echo $l['native_name']; ?></a>
			<?php }} ?>
			<ul>
			<?php foreach($langs as $l){ if(!$l['active']){ ?>
				<li><a href="<?php echo $l['url']; ?>"><span class="flag"><img src="<?php echo $l['country_flag_url']; ?>"></span><?php echo $l['native_name']; ?></a></li> 
			<?php }} ?>
			</ul>
		</div>
	</div>
</div>
</div>