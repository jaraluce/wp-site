<?php 
/**
 * 	Template Name: Home Page
*/
get_header(); 

$slides = CFS()->get('slides');
$nbr = count($slides); 
?>
<?php if (ICL_LANGUAGE_CODE == 'en') : ?>
	<a href="./cdn/docs/sequences_pre-targeting_en.pdf" target="_blank">
	<div id="top-bar">
		<h1>Automation</h1><!--
		--><i class="fa fa-heart" aria-hidden="true"></i><!--
		--><h1>Personalization</h1>
	</div><!--
	--><div id="download-guide">
		<p>free guide</p>
	</div>
	</a>
<?php elseif (ICL_LANGUAGE_CODE == 'es') : ?>
	<a href="./../cdn/docs/sequences_pre-targeting_es.pdf" target="_blank">
	<div id="top-bar">
		<h1>Automation</h1><!--
		--><i class="fa fa-heart" aria-hidden="true"></i><!--
		--><h1>Personalization</h1>
		<!-- <div class="free-guide corner-ribbon top-right shadow"><p>FREE<br />GUIDE</p></div> -->
	</div><!--
	--><div id="download-guide">
		<p>guía gratis</p>
	</div>
	</a>
<?php endif; ?>

<div id="slider-wrapper">
	<?php /*<div class="slider-mouse"></div>*/ ?>
	<div class="dotes">
	<?php if($nbr>1){ for($i=1; $i<=$nbr; $i++){ ?>
		<a class="go <?php echo $i; ?>" href="#"></a>
	<?php } } ?>
    </div>
	<div>
		<div id="main-slider" >
			<ul>
			<?php $i=0; foreach ($slides as $s) { $i++; ?>
				<li class="item<?php echo ($i==$nbr?' last':''); echo ($i==1?' first':''); ?>">
				<?php  echo parse_shortcode($s['slide_content']); ?>
				<!-- <?php echo parse_shortcode(CFS()->get('fd_animation')); ?> -->
				</li>
			<?php } ?>
			</ul>
		</div>
	</div>
</div>


<!-- <nav class="features-nav">
	<ul>
		<a href="#filter-discovery"><li class="nav-ft-1">1 <span>discover</span></li></a><a href="#unified-contacts"><li class="nav-ft-2">2 <span>engage</span></li></a><a href="#reach-in"><li class="nav-ft-3">3 <span>convert</span></li></a>
	</ul>	
</nav> -->

<div class="block-feature-wrapper">
	<div class="block block-feature" id="reach-in">
		<div class="wrapper flip">
			<div class="part img">
				<?php echo parse_shortcode(CFS()->get('uc_animation')); ?>
			</div>
			<div class="part last">
				<h3><?php echo CFS()->get('uc_title'); ?></h3>
				<?php echo CFS()->get('uc_text'); ?>
			</div>
		
		</div>
	</div>

	<div class="block block-feature" id="filter-discovery">
		<div class="wrapper"> 
			<div class="part">
				<h3><?php echo CFS()->get('fd_title'); ?></h3>
				<?php echo CFS()->get('fd_text'); ?>
			</div>
			<div class="part last img">
				<?php echo parse_shortcode(CFS()->get('fd_animation')); ?>
			</div>	
		</div>
	</div>

<!--  	<div class="block block-feature" id="reach-in">
		<div class="wrapper flip">
			<div class="part">
				<h3><?php echo CFS()->get('ri_title'); ?></h3>
				<?php echo CFS()->get('ri_text'); ?>
			</div>
			<div class="part last img">
				<?php echo parse_shortcode(CFS()->get('ri_animation')); ?>
			</div>
		</div>
	</div> -->

	<div class="block block-feature" id="featured-function">
		<div class="wrapper">
			<div class="part full">
				<?php echo CFS()->get('featured_function_text'); ?>
			</div>
			<div class="image">
				<p><?php echo CFS()->get('featured_function_image'); ?></p>
			</div>
		</div>
	</div>
</div>


<!-- Featured video -->
<div id="featured-video">
<video controls>
  <source src="<?php echo parse_shortcode(CFS()->get('video_link')); ?>" type="video/mp4">
</video>
<div class="poster"> 
	<div class="play"></div>
</div>

</div>

<!-- <div class="block" id="ft-main">
	<div class="wrapper">
		<?php echo CFS()->get('ft_main_text'); ?>
	</div>
</div> -->

<!-- <div class="block" id="hinter">
	<div class="wrapper flip">
		<div class="part last img">
			<img src="<?php echo parse_shortcode(CFS()->get('hinter_image')); ?>" title="Hinter - Chrome Extension" />
		</div>
		<div class="part">
			<h3><?php echo CFS()->get('hinter_title'); ?></h3>
			<?php echo CFS()->get('hinter_text'); ?>
		</div>
	</div>
</div> -->

<div id="join-us">
	<div class="image-block">
		<?php echo parse_shortcode(CFS()->get('join_us_image')); ?>
	</div>
	<div class="highlight-block">
		<?php echo parse_shortcode(CFS()->get('join_us')); ?>
	</div>
</div>

<?php $testimonials = CFS()->get('testimonials');
 if($testimonials){ ?>
<div id="testimonials">
	<div class="wrapper">
		<h3><?php _e('Testimonials','relevante'); ?></h3> 
		<div class="testimonials-slider">
			<ul>
			<?php foreach($testimonials as $t){ ?> 
				<li> 
					<div class="data">
						<div class="rating"><div style="width:<?php echo floatval($t['testi_rating'])/5*100 ?>%"></div></div>
						<h4><?php echo $t['testi_name']; ?> </h4> _ <span> <?php echo $t['testi_date']; ?></span>
						<?php echo $t['testi_text']; ?>
					</div>
					<img src="<?php echo $t['testi_image']; ?>" alt="">
				</li>
			<?php } ?> 
			</ul>
		</div>
		<button class="prev"></button>
		<button class="next"></button>
	</div>
</div>
<?php } ?>

<?php $members = CFS()->get('team_members');
 if($members){ ?>
<div id="our-team">
<div class="wrapper">
	<h3><?php _e('Our Team','relevante'); ?></h3>
	<?php foreach($members as $m){ ?> 
	<div class="member">
		<img src="<?php echo $m['member_image']; ?>">
		<div class="infos">
			<h4><?php echo $m['member_name']; ?></h4>
			<span><?php echo $m['member_job_title']; ?></span>
			<?php echo $m['member_description']; ?>
			<div class="social">
				<?php if($m['member_facebook_profil']){ ?><a target="blank" href="<?php echo $m['member_facebook_profil']; ?>" class="fb"></a><?php } if($m['member_twitter_profil']){ ?><a target="blank" href="<?php echo $m['member_twitter_profil']; ?>" class="tw"></a><?php } if($m['member_linkedin_profil']){ ?><a target="blank" href="<?php echo $m['member_linkedin_profil']; ?>" class="in"></a><?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
</div>
<?php } ?>
<?php get_footer();   ?>

<!-- <script type="text/javascript">
	$(document).ready(function(){
		$(".features-nav ul li").click(function(){
			$(".features-nav ul li.selected").removeClass("selected"),
			$(this).addClass("selected");
		});

		function featuresScrolledIntoView(elem) {
		    var docViewTop = jQuery(window).scrollTop();
		    var docViewBottom = docViewTop + jQuery(window).height();

		    var elemTop = jQuery(elem).offset().top;
		    var elemBottom = elemTop + jQuery(elem).height();

		    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
		}

		$(window).scroll(function(){
			if(featuresScrolledIntoView('#filter-discovery')
				|| featuresScrolledIntoView('#unified-contacts')
				|| featuresScrolledIntoView('#reach-in')) {
				$('nav.features-nav').fadeIn();
			} else {
				$('nav.features-nav').fadeOut();
			}

			if(featuresScrolledIntoView('#reach-in')) {
				$(".features-nav ul li.selected").removeClass("selected");
				$('.nav-ft-3').addClass('selected');
			}
			if(featuresScrolledIntoView('#unified-contacts')) {
				$(".features-nav ul li.selected").removeClass("selected");
				$('.nav-ft-2').addClass('selected');
			}
			if(featuresScrolledIntoView('#filter-discovery')) {
				$(".features-nav ul li.selected").removeClass("selected");
				$('.nav-ft-1').addClass('selected');
			}
		});

		window.setInterval(function () {
			$('#reach-in').toggleClass('intoView');
		}, 1500);

	});
</script> -->

<script type="text/javascript">
$( document ).ready(function() {
    mixpanel.track("Home page visit");
});


// Sync gif
$(window).load(function() {
    $('.preload').attr('src', function(i,a){
        $(this).attr('src','').removeClass('preload').attr('src',a);
    });
});

$("#main-slider .linkedin-signin-btn").click(function() {
	mixpanel.track("User signs in using main banner button");
});

$("#join-us .linkedin-signin-btn").click(function() {
	mixpanel.track("User signs in using bottom button");
});

$(".video-wrapper").click(function() {
	mixpanel.track("Video play");
});

$("#hinter").find('a').click(function() {
	mixpanel.track("Download Chrome extension");
});
</script>