<div id="footer">
	<div class="wrapper">
		<div class="part">
			<a href="<?php echo HOME_URL; ?>" id="ft-logo" ><img src="<?php echo THEME_URL; ?>/images/logo_grey.png"></a>
			<?php echo parse_shortcode(CFS()->get('footer_about_us', get_option('page_on_front'))); ?>
			<a href="<?php echo parse_shortcode(CFS()->get('footer_read_more', 2)); ?>" class="more"><span></span> <?php _e('Read More','relevante'); ?></a>
		</div>
		<div class="part m" id="useful-links">
			<h3><?php _e('USEFUL LINKS','relevante'); ?></h3>
			<?php $links = CFS()->get('useful_links', get_option('page_on_front'));
				foreach($links as $l){ ?>
				<a href="<?php echo parse_shortcode($l['footer_link']); ?>">- <?php echo $l['footer_link_title']; ?></a><br>
			<?php } ?>
		</div>
		<div class="part m">
			<h3><?php _e('Contact Info','relevante'); ?></h3>
			<a href="<?php echo CFS()->get('footer_group_link', get_option('page_on_front')); ?>" id="contactLinked" target="_blank"><span></span> <?php echo CFS()->get('footer_group_title', get_option('page_on_front')); ?></a><br>
			<a href="<?php echo CFS()->get('footer_twitter_link', get_option('page_on_front')); ?>" id="contactTwitter" target="_blank"><span></span> <?php echo CFS()->get('footer_twitter_text', get_option('page_on_front')); ?></a><br>
			<a href="mailto:<?php echo CFS()->get('footer_email', get_option('page_on_front')); ?>"  id="contactEmail"><span></span> <?php echo CFS()->get('footer_email', get_option('page_on_front')); ?></a>
		</div>
<?php
$msg = '';
if(!empty($_POST['message_contact']) and isset($_POST['go'])){
	$msg = __('Thank you, your message has been sent','relevante');
	$headers = 'From: Relevante <' . get_option('admin_email') . '>' . "\r\n";
	$email_data = $_POST['message_contact'];
	$html = (include 'inc/template-email.php');
	//echo $html; 
	wp_mail(get_option('admin_email'), 'Message from relevante.me', $html, $headers);
}
?>
		<div class="part">
			<!--<h3><?php _e('SEND US A MESSAGE','relevante'); ?></h3>-->
<?php if (!dynamic_sidebar('footer')) {} ?>
<!--
			<?php if($msg!=''){ ?>
				<div class="message success"><?php echo $msg; ?></div>
			<?php }else{ ?> 
				<form method="post">
					<textarea name="message_contact" placeholder="<?php _e('Type your message here','relevante'); ?>..."></textarea>
					<input type="submit" value="<?php _e('SUBMIT','relevante'); ?>" name="go">
				</form>
			<?php } ?>
-->
			<?php $langs = icl_get_languages('skip_missing=0'); ?>
			<div class="menu-item menu-item-type-post_type menu-item-object-page lang-switcher-small">
				<ul>
				<?php foreach($langs as $l){ if($l['active']){ ?>
					<li><a href="<?php echo $l['url']; ?>" class="current"><span class="flag"><img src="<?php echo $l['country_flag_url']; ?>"></span></span><?php echo $l['native_name']; ?></a></li>
				<?php }} ?>
				<?php foreach($langs as $l){ if(!$l['active']){ ?>
					<li><a href="<?php echo $l['url']; ?>"><span class="flag"><img src="<?php echo $l['country_flag_url']; ?>"></span><?php echo $l['native_name']; ?></a></li> 
				<?php }} ?>
				</ul>
			</div>
		</div>
		<div id="copy">
			<div> <img src="<?php echo THEME_URL; ?>/images/p2.jpg"> <img src="<?php echo THEME_URL; ?>/images/p1.jpg"> </div>
			<div id="mixpanel-partnership">
				<a href="https://mixpanel.com/f/partner"><img src="//cdn.mxpnl.com/site_media/images/partner/badge_light.png" alt="Mobile Analytics" /></a>
			</div>
			  &copy;<?php echo date('Y'); ?> <?php echo parse_shortcode(CFS()->get('footer_copyrights', get_option('page_on_front'))); ?>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript">
//Intercom
	function getParameterByName(name, url) {
    if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		    results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	var email;
	if (getParameterByName('email')) {
		email = getParameterByName('email');
	} else {
		email = null;
	}

	var referrer;
	if (jQuery.cookie('relevante_referrer') == null || jQuery.cookie('relevante_referrer') == "" ) {
		console.log('SET COOKIE')
		jQuery.cookie('relevante_referrer', document.referrer, { expires: 180 });
		referrer = document.referrer;
	} else {
		referrer = jQuery.cookie('relevante_referrer');
	}

$( document ).ready(function() {
    // set distinct_id after the mixpanel library has loaded
	mixpanel.init("dc2e431c6e52e50a035e106a78e663c2", {
	    loaded: function() {
	        distinct_id = mixpanel.get_distinct_id();
	    }
	});

	Intercom('update',
	{
		email: email,
		"referrer_email" : email,
		"referrer_url" : referrer
	});
});


$("#hd-login").click(function() {
	mixpanel.track("User signs in usign top page button");
});

$('.the-content .linkedin-signin-btn').click(function() {
	mixpanel.track("User signs in usign content page button");
});

$(".wysija-submit").click(function() {
	mixpanel.track("Clicked newsletter suscription");
});

$(".ccf-submit-button").click(function() {
	mixpanel.track("Contact form submitted");
});

$("#useful-links").click(function() {
	mixpanel.track("Clicked useful links");
});
</script>
<?php wp_footer();  ?>
<!-- <script type="text/javascript" src="https://www.prefinery.com/javascripts/widget2.js"></script> 
<script type='text/javascript' charset='utf-8'>
var prefinery_apply_options = { account: "relevante", beta_id: "6205", link_id: "prefinery_apply_link", link_class: "prefinery_apply", version: 2 }; 
Prefinery.apply(prefinery_apply_options); </script> -->
<!-- Agile CRM -->
<script type="text/javascript" src="https://relevanteme.agilecrm.com/stats/min/agile-min.js">
</script>
<script type="text/javascript" >
 _agile.set_account('3mfrrpvf2blh51j32gd9b9sat', 'relevanteme');
 _agile.track_page_view();
 _agile_execute_web_rules();
</script>
</body>
<html>