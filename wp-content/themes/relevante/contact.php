<?php 
/**
 * 	Template Name: Contact
*/
get_header(); the_post(); ?>
<div class="wrapper contact">  
	<article class="post"> 
		<h1 class="title"><?php  the_title(); ?></h1>
		<div class="the-content">
<?php
$msg = '';
if(!empty($_POST['frm_email']) and !empty($_POST['frm_message'])){
	$msg = __('Thank you, your message has been sent','relevante');
	$headers = 'From: Relevante <' . get_option('admin_email') . '>' . "\r\n";
	$email_data = '<b>Name</b> : '.$_POST['frm_name']."\n".
		'<br><b>Email</b> : '.$_POST['frm_email'].
		'<br><b>Phone</b> : '.$_POST['frm_phone'].
		'<br><b>Subject</b> : '.$_POST['frm_subject'].
		'<br><b>Message</b> : '.$_POST['frm_message'];
	$html = (include 'inc/template-email.php');
	//echo $html; 
	wp_mail(get_option('admin_email'), 'Message from relevante.me', $html, $headers);
}
?> 

<?php if($msg!=''){ ?>
	<div class="message success"><?php echo $msg; ?></div>
<?php }else{ ?>

			<?php the_content(); ?>
			<br>
			<form method="post" class="frm">
			<div class="row"> 
					<label><?php _e('Name','relevante'); ?></label>
					<input type="text" required name="frm_name" class="input small" placeholder="<?php _e('your full name','relevante'); ?>.">  
			</div>
			<div class="row">
					<label><?php _e('Email','relevante'); ?></label>
					<input type="email" required name="frm_email" class="input" placeholder="<?php _e('your email address','relevante'); ?>.">  
			</div>
			<div class="row">
					<label><?php _e('Phone','relevante'); ?></label>
					<input type="text" name="frm_phone" class="input" placeholder="<?php _e('your phone number','relevante'); ?>.">
			</div>
			<div class="row"> 
					<label><?php _e('Subject','relevante'); ?></label>
					<input type="text" name="frm_subject" class="input" placeholder="<?php _e('your message subject','relevante'); ?>."> 
			</div>
			<div class="row"> 
					<label class="area"><?php _e('Message','relevante'); ?></label>
					<textarea required name="frm_message" class="input" placeholder="<?php _e('your message','relevante'); ?>..."></textarea>
			</div>
			<input type="submit" value="<?php _e('SEND','relevante'); ?>" class="magenta-btn">
			
			</form>
<?php } ?>			
			<br>
			<br>
			<br>
			<br>

		</div>
	</article>
</div>
<?php get_footer(); ?>

<script type="text/javascript">
$( document ).ready(function() {
    mixpanel.track("Contact visit");
});
</script>