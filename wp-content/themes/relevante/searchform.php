<form role="search" method="get" class="search-form" action="<?php echo home_url( '/blog/' ); ?>">
	<label> 
		<input type="search" class="search-field" placeholder="<?php _e('Search','relevante'); ?>..." value="<?php echo get_search_query() ?>" name="s" />
	</label>
	<input type="submit" class="search-submit" value="<?php _e('Search','relevante'); ?>" />
</form>