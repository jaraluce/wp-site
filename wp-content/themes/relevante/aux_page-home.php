<?php 
/**
 * 	Template Name: Home Page
*/
get_header(); 

$slides = CFS()->get('slides');
$nbr = count($slides); 
?>
<div id="slider-wrapper">
	<?php /*<div class="slider-mouse"></div>*/ ?>
	<div class="dotes">
	<?php if($nbr>1){ for($i=1; $i<=$nbr; $i++){ ?>
		<a class="go <?php echo $i; ?>" href="#"></a>
	<?php } } ?>
    </div>
	<div class="wrapper">
		<div id="main-slider" >
			<ul>
			<?php $i=0; foreach ($slides as $s) { $i++; ?>
				<li class="item<?php echo ($i==$nbr?' last':''); echo ($i==1?' first':''); ?>">
				<?php  echo parse_shortcode($s['slide_content']); ?>
				</li>
			<?php } ?>
			</ul>
		</div>
	</div>
</div>

<div class="block" id="filter-discovery">
	<div class="wrapper"> 
		<div class="part">
			<h3><?php echo CFS()->get('fd_title'); ?></h3>
			<?php echo CFS()->get('fd_text'); ?>
		</div>
		<div class="part last img">
			<?php echo parse_shortcode(CFS()->get('fd_animation')); ?>
		</div>	
	</div>
</div>

<div class="block" id="unified-contacts">
	<div class="wrapper flip">
		<div class="part img">
			<?php echo parse_shortcode(CFS()->get('uc_animation')); ?>
		</div>
		<div class="part last">
			<h3><?php echo CFS()->get('uc_title'); ?></h3>
			<?php echo CFS()->get('uc_text'); ?>
		</div>
	
	</div>
</div>

<div class="block" id="reach-in">
	<div class="wrapper">
		<div class="part img">
			<?php echo parse_shortcode(CFS()->get('ri_animation')); ?>
		</div>
		<div class="part last">
			<h3><?php echo CFS()->get('ri_title'); ?></h3>
			<?php echo CFS()->get('ri_text'); ?>
		</div>
	</div>
</div>

<div id="join-us">
	<?php echo parse_shortcode(CFS()->get('join_us')); ?>
</div>

<div id="featured-video">
<video controls>
  <source src="<?php echo parse_shortcode(CFS()->get('video_link')); ?>" type="video/mp4">
</video>
<div class="poster"> 
	<div class="play"></div>
</div>

</div>
<?php $testimonials = CFS()->get('testimonials');
 if($testimonials){ ?>
<div id="testimonials">
	<div class="wrapper">
		<h3><?php _e('Testimonials','relevante'); ?></h3> 
		<div class="testimonials-slider">
			<ul>
			<?php foreach($testimonials as $t){ ?> 
				<li> 
					<div class="data">
						<div class="rating"><div style="width:<?php echo floatval($t['testi_rating'])/5*100 ?>%"></div></div>
						<h4><?php echo $t['testi_name']; ?> </h4> _ <span> <?php echo $t['testi_date']; ?></span>
						<?php echo $t['testi_text']; ?>
					</div>
					<img src="<?php echo $t['testi_image']; ?>" alt="">
				</li>
			<?php } ?> 
			</ul>
		</div>
		<button class="prev"></button>
		<button class="next"></button>
	</div>
</div>
<?php } ?>

<?php $members = CFS()->get('team_members');
 if($members){ ?>
<div id="our-team">
<div class="wrapper">
	<h3><?php _e('Our Team','relevante'); ?></h3>
	<?php foreach($members as $m){ ?> 
	<div class="member">
		<img src="<?php echo $m['member_image']; ?>">
		<div class="infos">
			<h4><?php echo $m['member_name']; ?></h4>
			<span><?php echo $m['member_job_title']; ?></span>
			<?php echo $m['member_description']; ?>
			<div class="social">
				<?php if($m['member_facebook_profil']){ ?><a target="blank" href="<?php echo $m['member_facebook_profil']; ?>" class="fb"></a><?php } if($m['member_twitter_profil']){ ?><a target="blank" href="<?php echo $m['member_twitter_profil']; ?>" class="tw"></a><?php } if($m['member_linkedin_profil']){ ?><a target="blank" href="<?php echo $m['member_linkedin_profil']; ?>" class="in"></a><?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
</div>
<?php } ?>
<?php get_footer();   ?>