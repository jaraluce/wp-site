<?php get_header(); ?>
<div class="wrapper blogpost">
	<?php if ( have_posts() ) :  ?>
		<?php  the_post(); ?>
			<article class="post">
				<h1 class="title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
				</h1>
				<div class="post-meta">
					<?php _e('Posted on','break'); ?> 
					<?php the_time('m/d/Y'); ?> <?php _e('in','break'); ?>
					<span class="category"><?php echo get_the_category_list(', '); ?></span> |
					<?php if( comments_open() ) : ?>
						<span class="comments-link">
							<?php comments_popup_link( __( 'No Comment', 'break' ), __( '1 Comment', 'break' ), __( '% Comments', 'break' ) );  
							?>
						</span>
					<?php endif; ?>
				</div>
				<div class="the-content"> 
					<?php
					if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					}
					the_content(); ?> 
					<?php //wp_link_pages(); ?>
				</div>
			<?php 
				if ( comments_open() || '0' != get_comments_number() )
					comments_template( '', true );
			?>
			</article> 
	<?php get_sidebar();  else : ?>
		<article class="post error">
			<h1 class="404"><?php _e('Nothing has been posted like that yet','relevante'); ?></h1>
		</article>
	<?php endif; ?>
	</div>
<?php get_footer(); ?> 

<script type="text/javascript">
$( document ).ready(function() {
    mixpanel.track("Blog: single post visit");
});
</script>